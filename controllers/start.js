'use strict';
const Telegram = require('telegram-node-bot'),
    LotteryController = require('./lottery'),
    ChanceController = require('./chance');

class StartController extends Telegram.TelegramBaseController {
    startHandler($) {
        const nombre = $.message.from.firstName;    
        $.sendMessage('*¿Que puede hacer este bot?*\n\n' +
            '*LotteryBot*🤖 esta creado con el fin de facilitar la busqueda del resultado de una lotería o chance, ya sea de manera especifica o los resultados del día actual.\n\n'+
            'Digita el comando /help para informacion sobre la funcionalidad de LoterryBot🤖.\n\n'+
            'Por favor comunicate con el creador @GersonJairG para cualquier inquietud o sugerencia.\n\n'+
            'Por ultimo recuerda que esta es una version beta de *LotteryBot*🤖 asi que cualquier sugerencia es una gran ayuda para mi.' , { parse_mode: 'Markdown' });
    }

    helpHandler($) {
        $.sendMessage('Puedo ayudarte a consultar el resultado de las loterías o chances de la siguiente manera:\n\n'+
        '"Hola" - Activarme para el paso a paso de la consulta del resultado.\n'+
        '/start - Solicitar el mensaje de bienvenida y contacto del creador.\n'+
        '/help - Solicitar explicacion de comandos o ayuda.', { parse_mode: 'Markdown' });
    }

    actionHandler($) {
        const lotteryController = new LotteryController();
        const chanceController = new ChanceController();
        const nombre = $.message.from.firstName;
        console.log(nombre);
        $.runInlineMenu({
            layout: 2,
            method: 'sendMessage',
            params: ['Hola ' + nombre +', ¿necesitas algo? por favor selecciona una opcion 😊:'],
            menu: [
                {
                    text: 'Loterías',
                    message: '¿Que tipo de lotería?',
                    layout: 2,
                    oneTimeKeyboard: true,
                    menu: [
                        {
                            text: 'Lotería especifica',                            
                            callback: (callbackQuery, message) => {
                                $.api.editMessageText('...🤖', { chat_id: $.chatId, message_id: message.messageId })
                                lotteryController.getLottery($);
                            }

                        },
                        {
                            text: 'Loterías de hoy',
                            callback: (callbackQuery, message) => {
                                $.api.editMessageText('...🤖', { chat_id: $.chatId, message_id: message.messageId })
                                lotteryController.allLottery($);
                            }
                        },
                    ]
                },
                {
                    text: 'Chances',
                    message: '¿Que tipo de chance?',
                    layout: 2,
                    oneTimeKeyboard: true,
                    menu: [
                        {
                            text: 'Chance en especifico',
                            callback: (callbackQuery, message) => {
                                $.api.editMessageText('...🤖', { chat_id: $.chatId, message_id: message.messageId })
                                chanceController.getChance($);
                            }

                        },
                        {
                            text: 'Chances de hoy',
                            callback: (callbackQuery, message) => {
                                $.api.editMessageText('...🤖', { chat_id: $.chatId, message_id: message.messageId })
                                chanceController.allChance($);
                            }
                        },
                    ]
                },

            ],
        });
    }

    get routes() {
        return {
            'startCommand': 'startHandler',
            'actionCommand': 'actionHandler',
            'helpCommand': 'helpHandler'
        };
    }
}

module.exports = StartController;