'use strict';
const Telegram = require('telegram-node-bot');

class OherWiseController extends Telegram.TelegramBaseController {
    handle($) {
        $.sendMessage('Lo siento, solo entiendo "Hola" por ahora 😔');
    }
}

module.exports = OherWiseController;