'use strict';
const Telegram = require('telegram-node-bot'),
    scrappingLottery = require('../scrapping/scrape-lottery'),
    commonMethods = require('../scrapping/commonMethods');

class LotteryController extends Telegram.TelegramBaseController {
    
    getResponse(lottery) {        
        return scrappingLottery.single(lottery).then((result) => {
            console.log('Lotería especifica: ');
            console.log(result);
            if (Object.keys(result).length > 0) {                
                return ('✨*' + result.nombre + ':*\n'
                    + '*Fecha:* ' + result.fecha + '\n'
                    + '*Resultado:* ' + result.resultado + '\n'
                    + '*Serie:* ' + result.serie);
            } else {
                return ('La lotería ingresada no existe');
            }
        }).catch((err) => {
            console.log(err);
        });

    }

    send(lottery, $) {
        // $.sendMessage('Por favor espera un poco mientras procesamos tu solicitud.');
        var flagMsg = true;
        setTimeout(() => {
            if(flagMsg){
                $.sendMessage('Por favor espera un poco mientras proceso tu solicitud 🤖');
            }
        }, 4000);
        this.getResponse(lottery).then((response) => {
            flagMsg = false;
            $.sendMessage(response, { parse_mode: 'Markdown' });        
        }).catch(() => {
            console.log(err);
        });
        
    }

    getLottery($) {        
        $.runInlineMenu({
            layout: 2,
            method: 'sendMessage',
            params: ['Por favor seleccione una loteria:'],
            menu: [
                {
                    text: 'Bogotá',
                    callback: () => { this.send('Bogotá', $); }
                },
                {
                    text: 'Boyacá',
                    callback: () => { this.send('Boyacá', $); }
                },
                {
                    text: 'Cauca',
                    callback: () => { this.send('Cauca', $); }
                },
                {
                    text: 'Cruz Roja',
                    callback: () => { this.send('Cruz Roja', $); }
                },
                {
                    text: 'Cundinamarca',
                    callback: () => { this.send('Cundinamarca', $); }
                },
                {
                    text: 'Extra de Colombia',
                    callback: () => { this.send('Extra de Colombia', $); }
                },
                {
                    text: 'Extra Navidad de Bogota',
                    callback: () => { this.send('Extra Navidad de Bogota', $); }
                },
                {
                    text: 'Huila',
                    callback: () => { this.send('Huila', $); }
                },
                {
                    text: 'Manizales',
                    callback: () => { this.send('Manizales', $); }
                },
                {
                    text: 'Medellín',
                    callback: () => { this.send('Medellín', $); }
                },
                {
                    text: 'Meta',
                    callback: () => { this.send('Meta', $); }
                },
                {
                    text: 'Quindío',
                    callback: () => { this.send('Quindío', $); }
                },
                {
                    text: 'Risaralda',
                    callback: () => { this.send('Risaralda', $); }
                },
                {
                    text: 'Santander',
                    callback: () => { this.send('Santander', $); }
                },
                {
                    text: 'Super Extra de Navidad',
                    callback: () => { this.send('Super Extra de Navidad', $); }
                },
                {
                    text: 'Super Millonaria de Colombia',
                    callback: () => { this.send('Super Millonaria de Colombia', $); }
                },
                {
                    text: 'Tolima',
                    callback: () => { this.send('Tolima', $); }
                },
                {
                    text: 'Valle',
                    callback: () => { this.send('Valle', $); }
                },
                {
                    text: 'Salir',
                    layout: 2,
                    callback: (callbackQuery, message) => {
                        $.api.editMessageText('Proceso terminado 😊', { chat_id: $.chatId, message_id: message.messageId })
                    }
                }
            ]
        })

    }

    allLottery($) {
        var flagMsg = true;
        setTimeout(() => {
            if(flagMsg){
                $.sendMessage('Por favor espera un poco mientras proceso tu solicitud 🤖');
            }
        }, 4000);
            return scrappingLottery.forDay(commonMethods.getDateToday())
                .then((response) => {
                    console.log('Loterías de hoy: ');
                    console.log(response);
                    if (!(response && response.length == 0)) {
                        let message = '*Loterias del dia de Hoy:*\n';
                        response.forEach(element => {
                            message += '✨*' + element.nombre + ':*\n'
                                + '*Fecha:* ' + element.fecha + '\n'
                                + '*Resultado:* ' + element.resultado + '\n'
                                + '*Serie:* ' + element.serie + '\n';
                        });
                        flagMsg = false;
                        $.sendMessage(message, { parse_mode: 'Markdown' });
                    } else {
                        flagMsg = false;
                        $.sendMessage('*Lo sentimos, no existen resultados de loterías el día de hoy.*', { parse_mode: 'Markdown' });                        
                    }
                }).catch((e) => {
                    console.log(e);
                });        
    }
}

module.exports = LotteryController;
