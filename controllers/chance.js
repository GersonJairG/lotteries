'use strict';
const Telegram = require('telegram-node-bot'),
    scrappingChance = require('../scrapping/scrape-chance'),
    commonMethods = require('../scrapping/commonMethods');

class ChanceController extends Telegram.TelegramBaseController {

    getResponse(chance) {
        return scrappingChance.singleChance(chance)
            .then((result) => {
                console.log('Chance especifico: ');
                console.log(result);
                if (Object.keys(result).length > 0) {
                    if (result.signo != null) {
                        return ('✨*' + result.nombre + ':*\n'
                            + '*Fecha:* ' + result.fecha + '\n'
                            + '*Resultado:* ' + result.resultado + '\n'
                            + '*Signo:* ' + result.signo);
                    }
                    return ('✨*' + result.nombre + ':*\n'
                        + '*Fecha:* ' + result.fecha + '\n'
                        + '*Resultado:* ' + result.resultado);

                } else {
                    return ('El chance ingresado no existe, o no hay resultados en el mes actual.');
                }
            }).catch((err) => {
                console.log(err);
            });
    }

    send(chance, $) {
        // $.sendMessage('Por favor espera un poco mientras procesamos tu solicitud.');
        var flagMsg = true;
        setTimeout(() => {
            if(flagMsg){
                $.sendMessage('Por favor espera un poco mientras proceso tu solicitud 🤖');
            }
        }, 4000);
        this.getResponse(chance).then((response) => {
            flagMsg = false;
            $.sendMessage(response, { parse_mode: 'Markdown' });        
        }).catch(() => {
            console.log(err);
        });        
    }

    getChance($) {
        $.runInlineMenu({
            layout: 2,
            method: 'sendMessage',
            params: ['Por favor seleccione un chance:'],
            menu: [
                {
                    text: 'Antioqueñita Día',
                    callback: () => { this.send('Antioqueñita Día', $); }
                },
                {
                    text: 'Antioqueñita Tarde',
                    callback: () => { this.send('Antioqueñita Tarde', $); }
                },
                {
                    text: 'Astro Luna',
                    callback: () => { this.send('Astro Luna', $); }
                },
                {
                    text: 'Astro Sol',
                    callback: () => { this.send('Astro Sol', $); }
                },
                {
                    text: 'Cafeterito Tarde',
                    callback: () => { this.send('Cafeterito Tarde', $); }
                },
                {
                    text: 'Cafeterito Noche',
                    callback: () => { this.send('Cafeterito Noche', $); }
                },
                {
                    text: 'Caribeña Día',
                    callback: () => { this.send('Caribeña Día', $); }
                },
                {
                    text: 'Caribeña Noche',
                    callback: () => { this.send('Caribeña Noche', $); }
                },
                {
                    text: 'Cash Three Día',
                    callback: () => { this.send('Cash Three Día', $); }
                },
                {
                    text: 'Cash Three Noche',
                    callback: () => { this.send('Cash Three Noche', $); }
                },
                {
                    text: 'Chontico Día',
                    callback: () => { this.send('Chontico Día', $); }
                },
                {
                    text: 'Chontico Noche',
                    callback: () => { this.send('Chontico Noche', $); }
                },
                {
                    text: 'Colono',
                    callback: () => { this.send('Colono', $); }
                },
                {
                    text: 'Culona',
                    callback: () => { this.send('Culona', $); }
                },
                {
                    text: 'Cuyabrito',
                    callback: () => { this.send('Cuyabrito', $); }
                },
                {
                    text: 'Dorado Mañana',
                    callback: () => { this.send('Dorado Mañana', $); }
                },
                {
                    text: 'Dorado Tarde',
                    callback: () => { this.send('Dorado Tarde', $); }
                },
                {
                    text: 'Dorado Noche',
                    callback: () => { this.send('Dorado Noche', $); }
                },
                {
                    text: 'Evening',
                    callback: () => { this.send('Evening', $); }
                },
                {
                    text: 'Fantastica Día',
                    callback: () => { this.send('Fantastica Día', $); }
                },
                {
                    text: 'Fantastica Noche',
                    callback: () => { this.send('Fantastica Noche', $); }
                },
                {
                    text: 'La Bolita Día',
                    callback: () => { this.send('La Bolita Día', $); }
                },
                {
                    text: 'La Bolita Noche',
                    callback: () => { this.send('La Bolita Noche', $); }
                },
                {
                    text: 'Motilon Tarde',
                    callback: () => { this.send('Motilon Tarde', $); }
                },
                {
                    text: 'Motilon Noche',
                    callback: () => { this.send('Motilon Noche', $); }
                },
                {
                    text: 'Paisa Lotto',
                    callback: () => { this.send('Paisa Lotto', $); }
                },
                {
                    text: 'Paisita Día',
                    callback: () => { this.send('Paisita Día', $); }
                },
                {
                    text: 'Paisita Noche',
                    callback: () => { this.send('Paisita Noche', $); }
                },
                {
                    text: 'Pijao de Oro',
                    callback: () => { this.send('Pijao de Oro', $); }
                },
                {
                    text: 'Play Four Día',
                    callback: () => { this.send('Play Four Día', $); }
                },
                {
                    text: 'Play Four Noche',
                    callback: () => { this.send('Play Four Noche', $); }
                },
                {
                    text: 'Samán de la Suerte',
                    callback: () => { this.send('Samán de la Suerte', $); }
                },
                {
                    text: 'Sinuano Día',
                    callback: () => { this.send('Sinuano Día', $); }
                },
                {
                    text: 'Sinuano Noche',
                    callback: () => { this.send('Sinuano Noche', $); }
                },
                {
                    text: 'SuperMillonario',
                    callback: () => { this.send('SuperMillonario', $); }
                },
                {
                    text: 'Win 4',
                    callback: () => { this.send('Win 4', $); }
                },
                {
                    text: 'Salir',
                    layout: 2,
                    callback: (callbackQuery, message) => {
                        $.api.editMessageText('Proceso terminado 😊', { chat_id: $.chatId, message_id: message.messageId })
                    }
                }
            ]
        })

    }

    allChance($) {
        // let text = $.message.text.split(' ').slice(1).join(' ');
        // if (!text) {
        // $.sendMessage('Por favor espera un poco mientras procesamos tu solicitud.');
        var flagMsg = true;
        setTimeout(() => {
            if(flagMsg){
                $.sendMessage('Por favor espera un poco mientras proceso tu solicitud 🤖');
            }
        }, 4000);
        return scrappingChance.forDayChance(commonMethods.getDateToday())
            .then((response) => {
                console.log('Chances de hoy: ');
                console.log(response);
                if (!(response && response.length == 0)) {
                    let message = '*Chances del dia de Hoy:*\n \n';
                    response.forEach(element => {
                        if (element.signo != null) {
                            message += '✨*' + element.nombre + ':*\n'
                                + '*Fecha:* ' + element.fecha + '\n'
                                + '*Resultado:* ' + element.resultado + '\n'
                                + '*Signo:* ' + element.signo + '\n'
                                + '\n';
                        } else {
                            message += '✨*' + element.nombre + ':*\n'
                                + '*Fecha:* ' + element.fecha + '\n'
                                + '*Resultado:* ' + element.resultado + '\n'
                                + '\n';
                        }
                    });
                    flagMsg = false;
                    $.sendMessage(message, { parse_mode: 'Markdown' });
                } else {
                    flagMsg = false;
                    $.sendMessage('*Lo sentimos, no existen resultados de loterías el día de hoy.*', { parse_mode: 'Markdown' });
                }
            }).catch((e) => {
                console.log(e);
            });
        // } else {
        //     return $.sendMessage('Disculpa, el comando /all funciona sin palabras adicionales 😅');
        // }
    }
}

module.exports = ChanceController;