function normalize(str) {
    var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",
        to = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
        mapping = {};

    for (var i = 0, j = from.length; i < j; i++) {
        mapping[from.charAt(i)] = to.charAt(i);
    }

    var ret = [];
    for (var i = 0, j = str.length; i < j; i++) {
        var c = str.charAt(i);
        if (mapping.hasOwnProperty(str.charAt(i)))
            ret.push(mapping[c]);
        else
            ret.push(c);
    }
    return ret.join('').toLowerCase();
};

function withoutSpacesSides(str) {
    String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, ""); };
    return str.trim();
}

var replaceSpaces = (str) =>{
    return str.replace(/\s+/g, '-');
}

module.exports.normalize = normalize;
module.exports.withoutSpacesSides = withoutSpacesSides;