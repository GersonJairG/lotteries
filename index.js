'use strict';
const PORT = process.env.PORT || 5000,
    commonMethods = require('./scrapping/commonMethods');

const Telegram = require('telegram-node-bot'),
    tg = new Telegram.Telegram('626995915:AAEoh31smnsNsZGiRfkNZb3Gu2qpnDU6dJM', {
        workers: 1,
        webAdmin: {            
            port: PORT            
        }
    });

const OtherwiseController = require('./controllers/otherwise'),
    StartController = require('./controllers/start');

commonMethods.wakeUpDyno();
tg.router.when(new Telegram.TextCommand('/start', 'startCommand'), new StartController())
    .when(new Telegram.TextCommand('/help', 'helpCommand'), new StartController())
    .when(new Telegram.TextCommand('Hola', 'actionCommand'), new StartController())
    .when(new Telegram.TextCommand('hola', 'actionCommand'), new StartController()) // El mismo comando pero en Minuscula
    .when(new Telegram.TextCommand('HOLA', 'actionCommand'), new StartController()) // El mismo comando pero en Mayuscula
    .otherwise(new OtherwiseController());