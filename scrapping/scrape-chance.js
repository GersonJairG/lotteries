const request = require('request-promise'),
    cheerio = require('cheerio'),
    commonMethods = require('./commonMethods'),
    normalize = require('../normalize/normalize');

singleChance = (chance) => {
    let chanceNormalize = commonMethods.isValidUrl(chance, 'Chance');
    const url = 'https://www.nacionalloteria.com/colombia/' + chanceNormalize + '.php';

    var options = {
        uri: url,
        transform: (html) => {
            return cheerio.load(html);
        }
    }

    return request(options).then(($) => {
        const details = $('.underline').first().text();
        console.log(details);
        const number = $('.label.label-estr.label-numero').eq(0).text();
        if (url.indexOf(normalize.normalize('Astro')) != -1) {
            const sign = $('.label.label-estr.label-numero').eq(1).text();
            console.log(extractSingleChance(details, number, sign));
            return extractSingleChance(details, number, sign);
        } else {
            // console.log(extractSingleChance(details, number, null));
            return extractSingleChance(details, number, null);
        }
    }).catch((err) => {
        console.log('ERROR INTERNO');
        console.log(err);
        return {};
    })
}

forDayChance = (date) => {
    const url = 'https://www.nacionalloteria.com/colombia/chance.php?del-dia=' + date;

    var options = {
        uri: url,
        transform: (html) => {
            return cheerio.load(html);
        }
    }

    return request(options).then(($) => {
        const val = $('.alert.alert-danger.mt-md.text-gr-esc');
        if (val.length != 0) {
            // console.log(val.text());
            console.log('[]');
            return []
        } else {
            const names = [],
                dates = [];
            $('.underline').each((i, el) => {
                let element = $(el).text();
                // let content = element.split(' de hoy ');
                let content = commonMethods.splitText(element);
                names.push(content[0]);
                dates.push(content[1]);
            });

            let numbers = [], signs = [];
            var backNumber = false;
            $('.label.label-estr.label-numero').each((i, el) => {
                const element = $(el).text();
                if (commonMethods.hasNumber(element) == 1) {
                    if (backNumber) {
                        signs.push('');
                        numbers.push(element);
                    } else numbers.push(element);

                    backNumber = true;
                } else if (commonMethods.hasLetter(element) == 1) {
                    signs.push(element);
                    backNumber = false;
                }
            });
            if (signs.length < numbers.length) {
                signs.push('');
            }
            // console.log(extractAllChance(names, dates, numbers, signs));
            return extractAllChance(names, dates, numbers, signs);
        }
    }).catch((err) => {
        console.log('ERROR INTERNO');
        console.log(err);
    })
}

extractSingleChance = (details, number, sign) => {
    let result = {}, content = commonMethods.splitText(details);

    result.nombre = content[0];
    result.fecha = content[1];
    result.resultado = number;
    if (sign)
        result.signo = sign;

    return result;
}

extractAllChance = (names, dates, numbers, signs) => {
    if (names && names.length == 0) return [];

    let process = [];
    let final = [];
    process = names.map((name, i) => ({
        nombre: name,
        fecha: dates[i],
        resultado: numbers[i],
        signo: signs[i]
    }));

    process.forEach(element => {
        if (element.nombre != '') {
            if (element.signo != '') {
                final.push(element);
            } else {
                final.push({
                    nombre: element.nombre,
                    fecha: element.fecha,
                    resultado: element.resultado
                });
            }
        }
    });
    return final;
}

// singleChance('astro SOL');
// forDayChance('2019-01-12');

module.exports.singleChance = singleChance;
module.exports.forDayChance = forDayChance;