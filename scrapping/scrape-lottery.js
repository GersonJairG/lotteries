const request = require('request-promise'),
    cheerio = require('cheerio'),
    commonMethods = require('./commonMethods');

single = (lottery) => {
    let lotteryNormalize = commonMethods.isValidUrl(lottery, 'Lottery');
    const url = 'https://www.nacionalloteria.com/colombia/' + lotteryNormalize + '.php';

    var options = {
        uri: url,
        transform: (html) => {
            return cheerio.load(html);
        }
    };

    return request(options).then(($) => {
        const details = $('.underline').first().text();
        const number = $('.label.label-estr.label-numero').eq(0).text();
        const serie = $('.label.label-estr.label-numero').eq(1).text();
        // console.log(extractSingle(details, number, serie));
        return extractSingle(details, number, serie);
    }).catch(function (err) {
        console.log('ERROR INTERNO');
        console.log(err);
        return {};
    })
}

forDay = (date) => {
    const url = 'https://www.nacionalloteria.com/colombia/resultados-loteria.php?del-dia=' + date;
    // console.log(url);
    var options = {
        uri: url,
        transform: (html) => {
            return cheerio.load(html);
        }
    }
    return request(options).then(($) => {
        const val = $('.alert.alert-danger.mt-md.text-gr-esc');
        if (val.length != 0) {
            // console.log(val.text());
            console.log('[]');
            return []
        } else {
            const details = [];
            $('.underline').each((i, el) => {
                const title = $(el).text();
                if (title.indexOf('Baloto') == -1)
                    details.push(title);
            });

            const numbers = [],
                series = [];
            $('.label.label-estr.label-numero').each((i, el) => {
                const element = $(el).text();
                if (element.length == 4)
                    numbers.push(element);
                else if (element.length > 2)
                    series.push(element);
            });
            
            // console.log(extractAll(details, numbers, series));
            return extractAll(details, numbers, series);
        }
    }).catch((err) => {
        console.log('ERROR INTERNO');
        console.log(err);        
    })

}

extractSingle = (details, number, serie) => {
    let result = {}, content = commonMethods.splitText(details);

    if (commonMethods.hasNumber(content[0]) == 1) {
        let nombre = content[0].substring(0, (content[0].length - 5));
        result.nombre = nombre;
    } else {
        result.nombre = content[0];
    }

    result.fecha = content[1];
    result.resultado = number;
    result.serie = serie;

    return result;
}

extractAll = (details, numbers, series) => {
    if (details && details.length == 0) return [];

    let names = [];
    let dates = [];
    let result = [];

    details.forEach(element => {
        let content = commonMethods.splitText(element);        
        dates.push(content[1]);
        if (commonMethods.hasNumber(content[0]) == 1)
            names.push(content[0].substring(0, (content[0].length - 5)));
        else
            names.push(content[0]);
    });

    result = names.map((name, i) => ({
        nombre: name,
        fecha: dates[i],
        resultado: numbers[i],
        serie: series[i]
    }));
    return result;
}

// single('Santander');

// var dia =  commonMethods.getDateToday();
// // console.log(dia);
// console.log('-----------------------');
// forDay(dia).then((response) => {
//     console.log(response);
// })

module.exports.single = single;
module.exports.forDay = forDay;