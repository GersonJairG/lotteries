const lotteryData = require('../data/urls/lotteries.json'),
    chanceData = require('../data/urls/chances.json'),
    normalize = require('../normalize/normalize'),
    request = require('request');

isValidUrl = (text, type) => {
    let url = '';
    let datos;
    if (type == 'Chance') datos = chanceData;
    else if (type == 'Lottery') datos = lotteryData;
    datos.some(element => {
        if (element.normalizado.indexOf(normalize.normalize(text)) != -1) {
            url = element.url;
            return element.url;
        }
    });
    return url;
}

hasNumber = (text) => {
    let hasNumber = 0,
        numbers = "0123456789";
    for (i = 0; i < text.length; i++) {
        if (numbers.indexOf(text.charAt(i), 0) != -1) hasNumber = 1;
    }
    return hasNumber;
}

hasLetter = (text) => {
    let hasLetter = 0,
        letters = "abcdefghyjklmnñopqrstuvwxyz";
    for (i = 0; i < text.length; i++) {
        if (letters.indexOf(text.charAt(i), 0) != -1) hasLetter = 1;
    }
    return hasLetter;
}

getDateToday = () => {
    const hoy = new Date(),
        // hoyNew = hoy.getTimezoneOffset() / 60 * (-1),
        dd = hoy.getDate(),
        mm = (hoy.getMonth() + 1) + '',
        yyyy = hoy.getFullYear();
    let mes = mm;
    if (mm == 1) {
        mes = '0' + mm;
    }

    console.log('fecha:' + yyyy + '-' + mes + '-' + dd);
    console.log('hoy: ' + hoy);
    let fecha = yyyy + '-' + mes + '-' + dd;
    return fecha;
}

splitText = (text) => {
    let content = [];
    if (text.indexOf('-') != -1) content = text.split(' - ');
    else if (text.indexOf('de hoy') != -1) content = text.split(' de hoy ');
    else if (text.indexOf('de ayer') != -1) content = text.split(' de ayer ');
    else console.log('ERROR: El nombre no contiene "-" ni "de hoy" o "de ayer".');

    content.forEach((element, i) => {
        content.splice(i, 1, normalize.withoutSpacesSides(element));
    });

    return content;
}

wakeUpDyno = () => {
    setInterval(function wakeUp() {
        request("https://bot-lottery.herokuapp.com/", function () {
            console.log("WAKE UP DYNO to 25 min");
        });
        // return reqTimer = setTimeout(wakeUp, 1200000);
    }, 1500000);
}

module.exports.isValidUrl = isValidUrl;
module.exports.hasNumber = hasNumber;
module.exports.hasLetter = hasLetter;
module.exports.getDateToday = getDateToday;
module.exports.splitText = splitText;
module.exports.wakeUpDyno = wakeUpDyno;